package com.academiamoviles.webinarroomapp.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.academiamoviles.webinarroomapp.R
import com.academiamoviles.webinarroomapp.data.Mascota
import com.academiamoviles.webinarroomapp.databinding.ItemMascotasBinding

class MascotaAdapter(var mascotas:List <Mascota> = listOf()) : RecyclerView.Adapter<MascotaAdapter.MascotaAdapterViewHolder>() {

    class MascotaAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private val binding : ItemMascotasBinding = ItemMascotasBinding.bind(itemView)

        fun bind(mascota:Mascota){

            binding.tvNombre.text = mascota.nombres
            binding.tvRaza.text = mascota.raza
            binding.tvPreferencias.text = mascota.preferencia
        }
    }

    fun updateData(mascotas :List <Mascota>){

        this.mascotas = mascotas
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MascotaAdapterViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mascotas,parent,false)
        return MascotaAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mascotas.size
    }

    override fun onBindViewHolder(holder: MascotaAdapterViewHolder, position: Int) {

        val mascota = mascotas[position]
        holder.bind(mascota)
    }
}