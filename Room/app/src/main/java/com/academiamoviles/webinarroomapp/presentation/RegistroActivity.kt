package com.academiamoviles.webinarroomapp.presentation

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import com.academiamoviles.webinarroomapp.R
import com.academiamoviles.webinarroomapp.data.AppDatabase
import com.academiamoviles.webinarroomapp.data.Mascota
import com.academiamoviles.webinarroomapp.databinding.ActivityMascotaBinding
import com.academiamoviles.webinarroomapp.databinding.ActivityRegistroBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegistroActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegistroBinding
    private val appDatabase  by lazy {
        AppDatabase.getInstance(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegistroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {

        binding.btnRegistrar.setOnClickListener {

            val nombres = binding.edtNombre.text.toString()
            val raza = binding.edtRaza.text.toString()
            val preferencia = binding.edtPreferencia.text.toString()

            val mascota = Mascota(0,nombres,raza,preferencia)

            lifecycleScope.launch {
                val respuesta = withContext(Dispatchers.IO){
                    appDatabase?.mascotaDao()?.insert(mascota)
                }

                if(respuesta.toInt() > 0)  mensaje("Mascota creado") else mensaje("Hubo un problema al registrar su masctoa")
                onBackPressed()
            }
        }
    }

    fun mensaje(mensaje:String){
        Toast.makeText(this,mensaje,Toast.LENGTH_SHORT).show()
    }



}